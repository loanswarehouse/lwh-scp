'use strict';

const Promise = require('bluebird');
const packagejs = require('../package.json');
const Request = require('request');
const fs = require('fs');
const moment = require('moment');

/** Goldmine problem indeces. 0 = OK and 1 - 4 show problems with names shown. */
const GM_STATUS_LIST = ["OK", "WebService", "MySQL", "GmSQL", "AppLoader"];
const DATA_HAPI = './data/hapi_data.json';
const DATA_GM = './data/goldmine_data.json';
const DATA_OTHER = './data/other_data.json';
const DATA_ISSUES_REPORTED = './data/issues_reported.json';
const DATA_ISSUES_COUNTER = './data/issues_counter.json';
const CHECK_INTERVAL = 30;   //In seconds.  Check services after CHECK_INTERVAL seconds.
const NEXT_REPORT_INTERVAL = 60; //In minutes, Only send new report if not already sent one within last 60 minutes 
const DELETE_OLD_REPORTS = 120; //In minutes, Delete older report after 120 minutes. 
//Numbers used by Comapi for texting
const TEXT_FROM = "447860004278";
const TEXT_TO_LIST = ["447955262536", "447856539874", "447508314740", "447980757373"];
const TEXT_TO_LIST_UAT = ["447773420344"];

/**
 * The Helper class is the main core class.
 */
class Helper {

    static isValidNumber(num, maxLength) {
        if (!num && num !== 0) return false;
        let numX = num.toString().trim();
        if (numX.length == 0 || numX.length > maxLength || isNaN(num)) return false;

        return true;
    }

    static timeDifference(dateStrA, dateStrB) {
        let time1 = new Date(dateStrA).getTime();
        let time2 = new Date(dateStrB).getTime();
        let diff = (time1 - time2) / 1000;
        diff = diff / 60; //in minutes
        return Math.abs(Math.round(diff));
    }

    /** Send Console log with datetime. */
    static log(logMsg, logObject) {
        let d = new Date();
        console.log('\x1b[33m%s\x1b[0m', d.toJSON().substring(0, 19) + ': ' + logMsg, logObject);
    }
    static error(logMsg, logObject) {
        let d = new Date();
        console.log('\x1b[31m%s\x1b[0m', d.toJSON().substring(0, 19) + ': ' + logMsg, logObject);
    }

    static sendResponse(res, status, msg, obj) {
        res.json({ version: packagejs.version, status: status, message: msg, result: obj });
    }

    static sendResponsePromisify(res, status, msg, obj) {
        return new Promise((resolve, reject) => {
            Helper.sendResponse(res, false, status, msg, obj);
            resolve();
        });
    }

    static jsonToString(json) {
        if (json !== null) {
            let jsonstr = this.toJSON(json);
            if (!jsonstr) {//convert to string
                return JSON.stringify(json);
            }
            return JSON.stringify(jsonstr);
        }
        return null;
    }

    static toJSON(data) {
        var ret = false;
        try { return JSON.parse(data); } catch (e) { ret = false; }

        return ret;
    }

    static createError(data, errorShort, errorMsg) {
        data.errorShort = errorShort;
        data.error = errorMsg;
    }

    static getIP(req) {
        let ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            (req.connection.socket ? req.connection.socket.remoteAddress : null);
        return ip;
    }

    static accessDenied(req, res) {
        let customerIP = Helper.getIP(req);
        if (process.env.IP_ALLOWED_LIST && customerIP) {
            if (!process.env.IP_ALLOWED_LIST.includes(customerIP)) {
                Helper.error('IP Blocked: ' + customerIP + ' - IP Allowd list: ', process.env.IP_ALLOWED_LIST);
                Helper.sendResponse(res, 401, 'Access Denied', "Access from your IP address not allowed.");
                return false;

            } else {
                Helper.log('IP Allowed:', customerIP);
            }
        } else {
            Helper.error('No IP Found:', customerIP);
        }

        return false;
    }

    static writeData(fileName, feedback, jsonData, caller) {
        return new Promise((resolve, reject) => {
            let json = this.jsonToString(jsonData);
            if (!json) {
                this.createError(feedback, 'Invalid JSON', 'A valid JSON required to store.');
                Helper.error('write error', feedback);
                return reject(feedback);
            }

            return fs.writeFile(fileName, json, 'utf8', (err) => {
                if (err) {
                    this.createError(feedback, 'Error Writing JSON', err);
                    Helper.error('write error', feedback);
                    return reject(feedback);
                } else {
                    feedback.caller = caller;
                    //Helper.log('write info', feedback );
                    return resolve(feedback);
                }
            });
        });
    }

    static readData(file, feedback, caller) {
        return new Promise((resolve, reject) => {
            fs.readFile(file, (err, content) => {
                if (err) {
                    this.createError(feedback, 'Error Reading JSON', err);
                    Helper.error('read error', feedback);
                    return reject(feedback);
                } else {
                    feedback.caller = caller;
                    feedback.data = this.toJSON(content);
                    if (!feedback.data) {
                        this.createError(feedback, 'Invalid JSON', 'No valid data saved yet.');
                        Helper.error('read error', feedback);
                        return reject(feedback);
                    }
                    //Helper.log('read file', feedback );
                    return resolve(feedback);
                }
            });
        });
    }

    static async readAllData(caller) {
        let hapiFeedback = {};
        let goldmineFeedback = {};
        let otherFeedback = {};
        await this.readData(DATA_HAPI, hapiFeedback, caller).catch((err) => {
            Helper.error('Reading hapi file error: ', err);
        });
        await this.readData(DATA_GM, goldmineFeedback, caller).catch((err) => {
            Helper.error('Reading goldmine file error: ', err);
        });
        await this.readData(DATA_OTHER, otherFeedback, caller).catch((err) => {
            Helper.error('Reading other file error: ', err);
        });

        return {
            hapi: (hapiFeedback) ? hapiFeedback : false,
            goldmine: (goldmineFeedback) ? goldmineFeedback : false,
            other: (otherFeedback) ? otherFeedback : false
        };
    }

    static checkServers() {
        let issueCounter = new IssueCounter();
        //run through different servers check after every x seconds
        let started = 0;
        let goldmineLastRequestReceived = new Date().toJSON();  //cache the most recent request received from goldmine  
        setInterval(() => {
            started++;
            //Keep checking services and collect data from them.
            //Then, check the data files and see if there are any issues in them

            //No need to chain other service, we don't care whichever runs first.
            Services.checkHapiApi(started).then(() => {
                //Helper.log('Hapi API', "Data processed successfully");
            });

            //check other services too. 
            Services.checkOther(started).then(() => {
                //Helper.log('Other API', "Data processed successfully");
            });


            //Check in this interval for goldmine last status request received
            Services.checkGoldmineStatusReceivingProblem(goldmineLastRequestReceived).then(() => {
                //Helper.log('Other API', "Data processed successfully");
            });

            //delete older issues
            Services.deleteOlderIssues();

        }, (1000 * CHECK_INTERVAL));

        //In every 10 seconds check for recorded issues in the files. 
        //The issues report should be running quicker in case of goldmine 
        //this is because goldmine issues are recorded by the external process not by us.
        let reportIssuesCount = 0;
        setInterval(() => {
            reportIssuesCount++;
            //Report issues
            Services.reportIssues(reportIssuesCount, issueCounter).then(() => {
                //Helper.log('Run Report', "Issues checked successfully");
            });

        }, (1000 * 10));
    }

}/**End class Helper */


class Services {

    static reportIssues(loopCount, issueCounter) {
        return new Promise((resolve, reject) => {
            async function runSilent() {
                let data = await Helper.readAllData('Checking Report');
                //Check each of the data files for errors.
                let allMessages = "";
                let issues = [];
                for (const key in data) {
                    if (data.hasOwnProperty(key)) {
                        const element = data[key];
                        //see if there is any new report available 

                        let skipIssues = moment().isBetween(moment('02:00', 'HH:mm'), moment('05:30', 'HH:mm'));
                        if (!skipIssues) {
                            let newIssue = element.data.new;
                            if (newIssue && newIssue.response) {
                                let reportDate = newIssue.runDate;
                                let response = element.data.new.response;
                                if (response && response.Status && response.Status > 0) {

                                    let message = `New ${key} issue detected on ${reportDate}. Problem with ${response.Name} status code ${response.Status}.`;

                                    if (key === 'goldmine' && response.Status === 999) {
                                        //Goldmine no update received
                                        message = "Hello Goldmine, plz talk to me. NO STATUS UPDATE received from Goldmine.";
                                    }

                                    let reportFormat = {
                                        area: key,
                                        problem: response.Name,
                                        status: response.Status,
                                        dated: new Date().toJSON(),
                                        message: message,
                                        loop: loopCount
                                    }
                                    issues.push(reportFormat);

                                    allMessages = message + " ";

                                }
                            }
                        }

                    }
                }

                if (issues.length > 0) {     //Issue exist
                    Services.processIssues(issues, allMessages, issueCounter);
                } else {                      //Issue not exist
                    //reset the old counter back to zero
                    issueCounter.resetIssueCounter();
                    Helper.log('NO ISSUES', 0);
                }

            }

            runSilent();
            resolve("check completed.");
        });
    }

    /*
    * Issues found, check them and process them.
    */
    static processIssues(issues, messageToSend, issueCounter) {
        Helper.error(`${issues.length} ISSUES FOUND: `, JSON.stringify(issues));

        //Check attempting rule for each of these errors
        if (!issueCounter.canReportIssue(issues)) {
            return Promise.resolve("Ignore");
        }

        //Report these issues but each should be reported no more than 1 time in an hour.
        //If there is any new issue then it will be reported too.      
        let data = {};
        return Helper.readData(DATA_ISSUES_REPORTED, data, 'ReportReader').then(() => {
            //Previous report exist, now compare the previous report with this new report
            let newIssuesToSave = [];//The list of actual issues to report
            messageToSend = '';//Reset message, only send relevent message. 
            issues.map((issue) => {
                let issueReportedAlready = Services.isThisIssueReportedAlready(data.data, issue);
                if (!issueReportedAlready) {
                    messageToSend += issue.message + " ";
                    newIssuesToSave.push(issue);
                }
            });
            //reset issueCounter so attempts counting can start back from zero NOT from where it left
            issueCounter.resetIssueCounter();

            return nowReportAndStoreThem(newIssuesToSave, messageToSend, data.data);

        })
            .catch((err) => {
                //reading previous data error, write as new
                Helper.log(`---SAVING NEW ISSUES REPORT---`, messageToSend);
                return nowReportAndStoreThem(issues, messageToSend, false);
            });

        function nowReportAndStoreThem(issuesToSave, messageToSend, oldIssues) {
            if (issuesToSave.length > 0) {
                //Helper.error("-Report Issues to Save: ", issuesToSave);                
                let newList = (oldIssues && oldIssues.length > 0) ? issuesToSave.concat(oldIssues) : issuesToSave;

                //save them
                return Helper.writeData(DATA_ISSUES_REPORTED, {}, newList, 'ReportWriter').then(() => {
                    if (messageToSend.length > 0) {
                        //send messages
                        let textResponse = {};
                        return TextAlert.sendText(messageToSend, textResponse)
                            .then((textResponseX) => {
                                Helper.error("---<<New Issue TEXT SENT>>--- ", messageToSend);
                                return Promise.resolve("New Report Sent");

                            }).catch((err) => {
                                Helper.error("-Problem sending text message: ", err);
                                return Promise.resolve("New Report Sent");
                            });

                    } else {
                        Helper.log("-New Issue Ignored: ", "Text already sent");
                        return Promise.resolve("Report Ignored");
                    }
                });

            } else {
                Helper.log("Issues reported recently: ", "Ignore them");
                return Promise.resolve("Issues were already reported.");
            }
        }
    }

    /**
     * Return the issue which was previously reported within the specific time
     * @param {*} fileData -the list of data from the issues_reported.json file
     * @param {*} newIssue -this issue to check against
     */
    static isThisIssueReportedAlready(fileData, newIssue) {
        //Check new issue in the existing file data list
        //The rules is:
        //Issue is the same but much older than an hour, report it.
        //Issue is the same and it's already reported within an hour, ignore it.
        //Issues aren't the same so store these new issues, report it.
        for (let index = 0; index < fileData.length; index++) {
            let oldIssue = fileData[index];
            if (oldIssue.area === newIssue.area && oldIssue.problem === newIssue.problem) {
                //same as previously reported problem
                //is this report already sent within the NEXT_REPORT_INTERVAL time frame. 
                if (Helper.timeDifference(oldIssue.dated, newIssue.dated) < NEXT_REPORT_INTERVAL) {
                    return oldIssue; //already reported within this time frame e.g. not older than an hour
                }
            }
        }
        //issue not found in the existing list
        return false;
    }

    /**
     * Older issues are no more required, so just delete them.
     */
    static deleteOlderIssues() {
        let data = {};
        return Helper.readData(DATA_ISSUES_REPORTED, data, 'DeleteReport').then(() => {
            let notToDeleteIssues = [];//The list of actual issues to report
            let currentTime = new Date().toJSON();
            data.data.map((issue) => {
                //Keep the new issues and the other ones will be overwritten
                if (Helper.timeDifference(issue.dated, currentTime) < DELETE_OLD_REPORTS) {
                    notToDeleteIssues.push(issue);
                }
            });

            if (notToDeleteIssues.length === data.data.length) {
                Helper.log("No older issues to delete", true);
                return Promise.resolve("No older issues to delete.");
            }

            return Helper.writeData(DATA_ISSUES_REPORTED, {}, notToDeleteIssues, 'ReportDelete').then(() => {
                Helper.error("--Older issues were deleted--", notToDeleteIssues);
                return Promise.resolve("Older Issues Deleted");
            });

        })
            .catch((err) => {
                Helper.log(`---No older issues to delete---`, true);
            });
    }

    /**
     * If response received from service endpoint, record it in the correct layout.
     * @param {*} response 
     */
    static createResponsePayload(response, status, name) {
        return { Status: status, Name: name, Result: response };
    }

    static checkHapiApi(loopCount) {
        let serviceResponse = {};
        //change back ip .5 to correct .1   
        return this._testEndpoint("https://ron.loanswarehouse.co.uk:40001/leads/last-lead")
            .then((result) => {

                serviceResponse = this.createResponsePayload('OK', 0, '.24 last-lead');
                //Hapi API is behind the above proxy and if it's stopped then the following JSON error will show
                //{"statusCode":502,"error":"Bad Gateway","message":"connect ECONNREFUSED 192.168.1.24:3000"}
                //We need to look for the proxy error too.
                if (result && result.includes('"statusCode":502')) {
                    //Proxy is complaining about the Hapi API
                    serviceResponse = this.createResponsePayload(result, 1, '.24 last-lead');
                }

                return this.checkData('HapiAPI', DATA_HAPI, loopCount, serviceResponse)
                    .then(() => {
                        //CRON PROJECT
                        //Now test cron project which is in the same box where happi project is but it's behind the proxy project 
                        //we already created a deeplink and added it in the proxy project
                        return this._testEndpoint("https://ron.loanswarehouse.co.uk:40001/scp-endpoint-test")
                            .then((cronProjectResponse) => {

                                //serviceResponse = this.createResponsePayload('OK', 0, 'CronProject');
                                //Hapi API is behind the above proxy and if it's stopped then the following JSON error will show
                                //{"statusCode":502,"error":"Bad Gateway","message":"connect ECONNREFUSED 192.168.1.24:3000"}
                                //We need to look for the proxy error too.
                                if (cronProjectResponse && (cronProjectResponse.includes('"statusCode":502') || cronProjectResponse.includes('"statusCode":404,"error":"Not Found"'))) {
                                    //Proxy is complaining about the Hapi API
                                    serviceResponse = this.createResponsePayload(cronProjectResponse, 1, 'CronProject');
                                    return this.checkData('CronProject', DATA_HAPI, loopCount, serviceResponse);
                                }

                            })
                            .catch((erra) => {
                                Helper.error('Problem reporting cronproject: ', erra);
                            })
                    })
            })
            .catch((err) => {
                //Proxy issue
                serviceResponse = this.createResponsePayload(err, 1, '.24 last-lead');
                return this.checkData('HapiAPI', DATA_HAPI, loopCount, serviceResponse);
            });
    }

    static checkOther(loopCount) {
        return checkScript();

        //Add all other services related to "Other" box under here and chain them together.

        function checkScript() {
            let serviceResponse = {};
            return Services._testEndpoint("http://admin.loanswarehouse.co.uk/", "text/html")
                .then((result) => {
                    serviceResponse = Services.createResponsePayload('Ember project loaded fine.', 0, 'Script');
                    return Services.checkData('Other API', DATA_OTHER, loopCount, serviceResponse);
                })
                .catch((err) => {
                    serviceResponse = Services.createResponsePayload(err, 1, 'Script');
                    return Services.checkData('Other API', DATA_OTHER, loopCount, serviceResponse);
                });
        }
    }


    static checkGoldmineTest(req, res) {
        //Mark will send: {"StatusAt":"2020-02-03T11:22:18.972541+00:00","Status":0,"Name":"OK"}
        if (Helper.accessDenied(req, res)) {
            return false;
        }

        let gmStatus = req.body;
        if (!gmStatus) {
            Helper.error('Testing Goldmine invalid request: ', gmStatus);
            Helper.sendResponse(res, 401, 'Invalid request', false);
            return false;

        } else if (typeof GM_STATUS_LIST[gmStatus.Status] === 'undefined') {
            Helper.error('Testing Goldmine wrong status: ', gmStatus.Status);
            Helper.sendResponse(res, 401, 'Invalid status', `No service found for status '${gmStatus.Status}'`);
            return false;

        } else if (GM_STATUS_LIST[gmStatus.Status] !== gmStatus.Name) {
            Helper.error('Testing Goldmine wrong name: ', gmStatus.Name);
            Helper.sendResponse(res, 401, 'Invalid status', `Status ${gmStatus.Status} should have a valid name '${GM_STATUS_LIST[gmStatus.Status]}'`);
            return false;
        }

        if (gmStatus.Status > 0) {
            Helper.error('Testing Goldmine Problem Noticed ', gmStatus.Status);
            Helper.sendResponse(res, 200, 'Test Issue Noticed', `LWH SCP will report the ${gmStatus.Name} issue.`);
        } else {
            Helper.log('Testing Goldmine OK hit: ', true);
            Helper.sendResponse(res, 200, 'Test Request processed.', 'OK');
        }
    }


    /**
     * Only External service writes to the goldmine file.
     * It shouldn't be called internally.
     */
    static checkGoldmine(req, res) {
        //Mark will send: {"StatusAt":"2020-02-03T11:22:18.972541+00:00","Status":0,"Name":"OK"}
        if (Helper.accessDenied(req, res)) {
            return false;
        }

        let gmStatus = req.body;
        if (!gmStatus) {
            Helper.error('Goldmine invalid request: ', gmStatus);
            Helper.sendResponse(res, 401, 'Invalid request', false);
            return false;

        } else if (typeof GM_STATUS_LIST[gmStatus.Status] === 'undefined') {
            Helper.error('Goldmine wrong status: ', gmStatus.Status);
            Helper.sendResponse(res, 401, 'Invalid status', `No service found for status '${gmStatus.Status}'`);
            return false;

        } else if (GM_STATUS_LIST[gmStatus.Status] !== gmStatus.Name) {
            Helper.error('Goldmine wrong name: ', gmStatus.Name);
            Helper.sendResponse(res, 401, 'Invalid status', `Status ${gmStatus.Status} should have a valid name '${GM_STATUS_LIST[gmStatus.Status]}'`);
            return false;
        }

        //Store the data      
        this.checkData('GoldmineAPI', DATA_GM, 0, gmStatus)
            .then(() => {

                if (gmStatus.Status > 0) {
                    Helper.error('Goldmine Problem Noticed ', gmStatus.Status);
                    Helper.sendResponse(res, 200, 'Issue Noticed', `LWH SCP will report the ${gmStatus.Name} issue.`);
                } else {
                    Helper.log('Goldmine OK hit: ', true);
                    Helper.sendResponse(res, 200, 'Request processed.', 'OK');
                }
            })
            .catch((err) => {
                Helper.error('Problem reporting goldmine issue: ', err);
                Helper.sendResponse(res, 500, 'Issue reporting problem.', err);
            });

    }

    static checkGoldmineStatusReceivingProblem(lastCheck) {
        let nowDate = new Date().toJSON();
        //We should be continuously receiving updates from goldmine server otherwise we should report them 
        //We need to wait at least x seconds before complaining about not receiving data from GM server       
        let data = {};
        return Helper.readData(DATA_GM, data, 'Viewing')
            .then(() => {
                let lastRunDate = data.data.new.runDate;
                let minutesAgo = Helper.timeDifference(lastRunDate, nowDate);
                if (minutesAgo > 4) {//In every 4 minutes check for goldmine status 
                    Helper.error('No goldmine status update received', " send problem alert once in an hour only");
                    return addIssue();
                }
                return Promise.resolve("OK");
            }).catch((err) => {
                //This section will only be hit once in life time of the app unless the goldmine data file has been cleared.
                let lastRunDate = lastCheck;
                let minutesAgo = Helper.timeDifference(lastRunDate, nowDate);
                Helper.error('Nothing in goldmine data file yet. Report to create in ', minutesAgo + " minutes");
                if (minutesAgo > 4) {
                    Helper.error('Goldmine Status Update Not Exist. ', " Problem created.");
                    return addIssue();
                }
                return Promise.resolve("Wait for 4 minutes");
            });

        function addIssue() {
            let serviceResponse = Services.createResponsePayload('Problem', 999, 'No Status Update');
            return Services.checkData('GoldmineAPI', DATA_GM, 0, serviceResponse)
        }

    }

    /**
     * Read and merge save the new data in the file.
     * @param {*} apiName 
     * @param {*} file 
     * @param {*} loopCount 
     * @param {*} serviceResponse 
     */
    static checkData(apiName, file, loopCount, serviceResponse) {
        let filePath = file;
        let data = {};

        return Helper.readData(filePath, data, 'Updating')
            .then(() => {
                return this.writeNewData(apiName, filePath, data.data, serviceResponse, loopCount);
            })
            .catch((err) => {
                //reading previous data error
                Helper.error(`No previous data ${apiName} - ${loopCount}`, err);
                return this.writeNewData(apiName, filePath, false, serviceResponse, loopCount);
            });
    }

    static writeNewData(apiName, filePath, previousData, serviceResponse, loopCount) {
        return new Promise((resolve, reject) => {

            let dataX = {};
            dataX.new = {
                runCount: loopCount,
                runDate: new Date().toJSON(),
                response: serviceResponse
            }

            //if previous new exist then move it
            if (previousData && previousData.new) {
                dataX.old = previousData.new;
            }

            return Helper.writeData(filePath, {}, dataX, 'Internal Check ' + apiName)
                .then(() => {
                    //Helper.log(`check ${apiName} data`, false);
                    resolve(dataX);
                })
                .catch((err) => {
                    Helper.error(`Error writing ${apiName} data`, err);
                    reject("Problem writing data");
                });

        });
    }


    /**
     * Send JSON test request to an endpoint to test response time.
     */
    static _testEndpoint(requestUrl, reserve = 'application/json') {
        let requestBody = JSON.stringify({ statusCheck: 1 });

        return new Promise(function (resolve, reject) {
            Request(
                {
                    method: 'GET',
                    url: requestUrl,
                    headers: {
                        'Content-Type': reserve,
                        'Cache-Control': 'no-cache'
                    },
                    body: requestBody,
                },
                (error, response, body) => {
                    if (error) {
                        return reject(error);
                    }
                    return resolve(body);
                }
            );

        });

    }/**End _testEndpoint */

}


//This is the layout of how the data will be stored in the issues_counter file
const ISSUES_COUNTER_RULES = [
    { area: "hapi", problem: ".24 last-lead", totalAttemptsReguired: 6, totalAttempted: 0, lastAttemptDate: "" },
    { area: "hapi", problem: "CronProject", totalAttemptsReguired: 6, totalAttempted: 0, lastAttemptDate: "" },
    { area: "Other API", problem: "Script", totalAttemptsReguired: 1 },
    { area: "goldmine", problem: "WebService", totalAttemptsReguired: 24, totalAttempted: 0, lastAttemptDate: "" },
    { area: "goldmine", problem: "MySQL", totalAttemptsReguired: 24, totalAttempted: 0, lastAttemptDate: "" },
    { area: "goldmine", problem: "GmSQL", totalAttemptsReguired: 24, totalAttempted: 0, lastAttemptDate: "" },
    { area: "goldmine", problem: "AppLoader", totalAttemptsReguired: 24, totalAttempted: 0, lastAttemptDate: "" },
    { area: "goldmine", problem: "No Status Update", totalAttemptsReguired: 24, totalAttempted: 0, lastAttemptDate: "" }
];
/**
 * Track the record of all the error alerts.
 * Apply reattempt proceedure on x service
 */
class IssueCounter {

    constructor() {
        this.recordedIssues = false;
        this.counterIndex = 0;   //index of the current ISSUES_COUNTER_RULES
    }

    getIssueCounterRules(area, problem) {
        this.counterIndex = 0;
        let rule = false;
        for (let index = 0; index < ISSUES_COUNTER_RULES.length; index++) {
            const counter = ISSUES_COUNTER_RULES[index];
            if (counter.area == area && counter.problem == problem) {
                rule = counter;
                this.counterIndex = index;
            }

        }
        return rule;
    }

    canReportIssue(issues) {
        //this issue rules should be created already.
        let issue = issues[0];
        let issueCounterRules = this.getIssueCounterRules(issue.area, issue.problem);
        if (!issueCounterRules) {
            //rules should exist otherwise do not allow sending texts
            Helper.error(`This issue has no rules, add rules in ISSUES_COUNTER_RULES object for: `, `${issue.area} ${issue.problem}`);
            return false;
        }

        //THIS WILL ALWAYS SEND WITHOUT ATTEMPT when rule for the totalAttemptsReguired is 0.
        if (issueCounterRules.totalAttemptsReguired == 0) {
            Helper.log(`No need for attempt retrying: `, `${issue.area} ${issue.problem}`);
            return true;

        } else if (issueCounterRules.totalAttempted >= issueCounterRules.totalAttemptsReguired) {
            Helper.log(`Attempt reached ${issueCounterRules.totalAttempted} for: `, `${issue.area} ${issue.problem}`);
            issueCounterRules.totalAttempted = 0; //reset to zero
            issueCounterRules.lastAttemptDate = new Date().toJSON();
            this.saveIssueCounter(issueCounterRules);
            return true;

        } else {
            //increase total attempt
            issueCounterRules.totalAttempted++;
            issueCounterRules.lastAttemptDate = new Date().toJSON();
            Helper.log(`Attempt not reached: ${issueCounterRules.totalAttempted}`, `${issue.area} ${issue.problem}`);
            this.saveIssueCounter(issueCounterRules);
            return false;

        }
    }

    saveIssueCounter(issueCounterRule) {
        ISSUES_COUNTER_RULES[this.counterIndex] = issueCounterRule;
    }

    resetIssueCounter() {
        ISSUES_COUNTER_RULES[this.counterIndex].totalAttempted = 0;
    }
}

/**
 * Class TextAlert sending text message.
 */
class TextAlert {

    static sendText(message, data) {
        let Text_everyone = TEXT_TO_LIST;
        if (process.env.NODE_ENV !== 'production') {
            Text_everyone = TEXT_TO_LIST_UAT;
            message += message + " -SAI TESTING";
            Helper.log("Ignore texting: ", message);
            return Promise.resolve("Text Sent ignored");
        }

        return new Promise((resolve, reject) => {

            // Setup Comapi request JSON - payload
            let listOfSendTo = [];
            Text_everyone.forEach(destinationNumber => {
                const dataRequest = {
                    body: message,
                    to: {
                        phoneNumber: destinationNumber,
                    },
                    rules: [
                        "sms",
                    ],
                    channelOptions: {
                        sms: {
                            from: TEXT_FROM,
                            allowUnicode: true,
                        },
                    },
                };

                listOfSendTo.push(dataRequest);
            });

            let url = 'https://api.comapi.com/apispaces/' + process.env.TEXTS_MS_COMAPI_SPACEID + '/messages/batch';
            let header = {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'accept': 'application/json',
                'authorization': 'Bearer ' + process.env.TEXTS_MS_COMAPI_TOKEN,
            }

            //Helper.error(`Text URL: ${url}`, header);
            //Helper.error(`Text listOfSendTo:`, JSON.stringify(listOfSendTo));
            // Call Comapi  'One' API
            return Request({
                method: 'POST',
                url: url,
                headers: header,
                body: listOfSendTo,
                json: true,
            }, function (error, httpResponse, body) {

                data.comapiSmsResponse = body;
                if (error) {
                    Helper.error(`Sending TEXT ERROR: `, error);
                    return reject(error);

                } else if (typeof body !== 'object') {
                    let invalidError = {
                        body: body,
                        options: 'expected a valid JSON response',
                        statusCode: httpResponse.statusCode
                    }
                    Helper.error(`Sending TEXT INVALID: `, invalidError);
                    return reject(invalidError);

                } else if (httpResponse.statusCode == 400) {
                    Helper.error(`INVALID TEXT RESPONSE ${httpResponse.statusCode} `, body);
                    return reject(body);

                } else {
                    return resolve("Text Sent");
                }

            });
        });
    }
}

module.exports = { Helper, Services, GM_STATUS_LIST, TextAlert };