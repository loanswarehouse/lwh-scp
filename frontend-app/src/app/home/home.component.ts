import { Component, OnInit } from '@angular/core';
import {HelperService} from '../services/helper.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public Helper: HelperService) {
  }
  public audio = new Audio();
  public data: any = false;

  ngOnInit() {
      this.run();
  }

  /**
   * Keep checking backend for any errors.
   */
  public run(){
      let seconds = 3;
      setInterval(() => {
          this.Helper.callGet("get-details", this, "data");
          this.decisionEngine();
      }, 1000 * seconds);
  }

  public issueGMServer: any = false;
  public issueHapiServer: any = false;
  public issueOtherServer: any = false;

  public resetAlerts(){
    this.issueGMServer = false;
    this.issueHapiServer = false;
    this.issueOtherServer = false;
  }

  public decisionEngine(){
      let messages = [];
      if(this.data){
          //No need to see the old record, check the fresh status
          this.resetAlerts();
          for (const key in this.data) {
            if (this.data.hasOwnProperty(key)) {
              const element = this.data[key];
              //see if there is any new report available 
              let newIssue = element.data.new;
              if(newIssue && newIssue.response){
                  let response = element.data.new.response;
                  if(response && response.Status && response.Status > 0){ 
                        let reportDate = newIssue.runDate;                     
                        //Write the issue to correct box
                        let issue = {Status: response.Status, Name: response.Name, Dated: reportDate};
                        if(key === 'hapi'){
                          this.issueHapiServer = issue;
                          messages.push(issue);
                        }else if(key === 'goldmine'){
                          this.issueGMServer = issue;
                          messages.push(issue);
                        }else if(key === 'other'){
                          this.issueOtherServer = issue;
                          messages.push(issue);
                        }                      
                  }
              }  
              
            }
          }//End for loop        
      }//end if  
      
      //Issues to output
      if(messages.length > 0){
          console.log(messages);
          this.playAudio();
      }else{
          this.pauseAudio();
      }

  }//end decisionEngine

  playAudio(){
    if(this.audio.paused){
      this.audio.src = "./assets/sound.mp3";
      this.audio.load();
      this.audio.play();
    }
  }
  pauseAudio(){
    this.audio.pause();
  }

}
