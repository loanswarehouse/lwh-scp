import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  //private ApiUrl = 'http://localhost:3001/api/endpoint/';  // UAT URL
  private ApiUrl = 'http://34.251.179.60/api/endpoint/';  // LIVE URL

  constructor(private router:Router, private http:Http) { }

  callGet(part, className, objectToSetTo){        
      let url = this.ApiUrl+part;

      let api = this.http.get(url).toPromise()
          .then(HelperService.extractData)
          .catch(HelperService.handleErrorPromise);

      api.then(response => {
            if(response){
                className[objectToSetTo] = response.body;
            }
        });
  }

  goTo(urlPart){
      this.router.navigate([urlPart]);
  }
    
  static handleErrorPromise (error: Response) {
      let msg = '';
      return HelperService.prepareResponse(false, msg, error);
  }

  static prepareResponse(isPass, yourMsg, output){
      return {pass: isPass, msg: yourMsg, body: output};
  }

  static prepareJsonHeader(){
      let headers = new Headers({
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'Pragma': 'no-cache',
          'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
      });
      let options = new RequestOptions({ headers: headers });
      return options;
  }
  
  static extractData(res: Response) {
      var output = res.text();
      var customMsg = "";
      var isPass = false;
      let jsonFormat = HelperService.isJSON(output);
      let result = "";

      if (jsonFormat) {
          //console.log(jsonFormat);
          if(jsonFormat === true){ //not yet loaded from backend
                customMsg = "Loading from backend... ";

          }else if(!jsonFormat.status || jsonFormat.status != 200){ //not yet loaded from backend
                customMsg = jsonFormat.message;

          }else if(jsonFormat.status === 200){ //exist
                customMsg = jsonFormat.message;
                isPass = true;
                result = jsonFormat.result;

          }else{ //not exist
                customMsg       = jsonFormat.message;
          }

      }else{
          customMsg = "Invalid data format response from server. Should be JSON";
          console.log("-------backend response should be JSON format-------");
          console.log(res);
          result   = output;
      }
      //this.result = res.json();
      return HelperService.prepareResponse(isPass, customMsg, result);
  }
  
  static isJSON(data) {
      let ret = false;
      try {
            return JSON.parse(data);
      }catch(e) {
              ret = false;
      }
      return ret;
  }

}
