
'use strict';
const Helper = require('../core/helper').Helper;
const Services = require('../core/helper').Services;
module.exports = (router) => {
    router.get('/get-details', async (req, res) => {
        //we will get the results from the local file that the cronjobs have just written them to.
        let data = await Helper.readAllData('Viewing')
        Helper.sendResponse(res, 200, 'OK', data);
    });

    //Get a single dataset
    router.get('/get/:id', (req, res) => {
        // Retrieve the tag from our URL path
        var id = req.params.id;
        const list = ["hapi","goldmine","other"];
        if(!list.includes(id)){
            Helper.sendResponse(res, 401, 'Error', "Request not found.");
        }

        let data = {};
        Helper.readData(`./data/${id}_data.json`, data, 'Viewing')
        .then(() => {
            Helper.sendResponse(res, 200, 'OK', data);
        }).catch((err) => {
            Helper.sendResponse(res, 500, 'Error', err);
        });

    });

    router.post('/goldmine-server-status', (req, res) => {Services.checkGoldmine(req, res);});
    router.post('/goldmine-server-status-testing', (req, res) => {Services.checkGoldmineTest(req, res);});
  
    return router;
  }