'use strict';

require('dotenv').config();
const express       = require('express');
const bodyParser    = require('body-parser');

const Helper    = (require('./core/helper')).Helper;

const port = process.env.WEB_PORT;

const server  = express();
var router    = express.Router();

// Import Routers Routes
const endpoints         = require('./routes/process')(router);

// Middleware setup
server.use(bodyParser.urlencoded( {limit: '30mb', extended: true} ));   // parse application/x-www-form-urlencoded
server.use(bodyParser.json( {limit: '30mb', extended: true} ));         // parse application/json configuration
//server.use('/', express.static(__dirname + '/frontend/'));        		/* Provide static directory for frontend */

server.use(function(req,res,next){
	res.header('Access-Control-Allow-Origin','*'); //To be using only single domain.
	res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers','Content-Type');
	//res.header('Access-Control-Allow-Origin', 'http://admin.loanswarehouse.co.uk');
	next();
});

//JSON contents type only
server.use(function(req,res,next){
	res.header('Content-Type', 'application/json');
	next();
});

//turn off caching
server.use((req, res, next) => {
	res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
	res.header('Expires', '-1');
	res.header('Pragma', 'no-cache');
	next();
});

// express apply routing
server.use('/api/endpoint', endpoints);

// check in production mod or dev mode
if (process.env.NODE_ENV === 'production') {
	Helper.log('info', 'Server is running in production mode' );
}else{
	Helper.log('info', 'Server is running in dev mode' );
}

// Start Server: Listen on port xyz
server.listen(port, () => {
	Helper.log('info', 'Running on port '+port );
		
	// start cronjob
	Helper.checkServers();
});
